<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author Jorge Loyola
 */
class Home {
    
    public function index() {
        if(isset($_GET["ajax"]) and $_GET["ajax"]=="si" and isset($_GET["function"]) and $_GET["function"]!=""){
            //pregunto que funcion de ajax
            switch ($_GET["function"]) {
                case "eliminar_usuario":
                    //llamo al controlador eliminar usuario y retorno ok o error
                    $respuesta= Usuarios::delete_ajax($_GET["id"]);
                    //echo var_dump($respuesta);
                    break;
                default :
                    echo "error";
            }
        } else {
            include "Views/index.php";
        }
    }
}

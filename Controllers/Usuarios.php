<?php
require_once(__DIR__ . "/Validacion.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuarios
 *
 * @author Jorge Loyola
 */
class Usuarios {
//    
//    protected $UsuariosModel;
//
//
//    public function __construct() {
//        $this->UsuariosModel=new UsuariosModel();
//    }


    static public function login($data) {
        if(isset($data)){
            $validacion=new Validacion();
            if($validacion::validar_email($data["email"])){
                if($validacion::validar_clave($data["clave"])){
                    $existe= UsuariosModel::get_data("count(id) as total", "email_usuario", $data["email"]);
                    if($existe["total"]>0){
                        $respuesta = UsuariosModel::get_data("clave_usuario, id, nombre_usuario, apellidop_usuario", "email_usuario", $data["email"]);
                        if($respuesta["clave_usuario"]==sha1($data["clave"])){
                            //inicia sesion
                            session_start();
                            $_SESSION["usuario"]["id"]=$respuesta["id"];
                            $_SESSION["usuario"]["email"]=$data["email"];
                            $_SESSION["usuario"]["nombre"]=$respuesta["nombre_usuario"]." ".$respuesta["apellidop_usuario"];
                            $_SESSION["usuario"]["logueado"]="si";
                            echo '<script>window.location="index.php?go=listar"</script>';
                        } else {
                            echo '<div class="alert alert-warning" role="alert">La Contraseña ingresada no es correcta!</div>';
                        }
                    } else {
                        echo '<div class="alert alert-warning" role="alert">El Email ingresado no se encuentra registrado en el sistema!</div>';
                    }
                } else {
                    echo '<div class="alert alert-warning" role="alert">La Clave ingresada no es válida!</div>';
                }
            } else {
                echo '<div class="alert alert-warning" role="alert">El Email ingresado no es válido!</div>';
            }
        } else {
            echo '<div class="alert alert-danger" role="alert">Error al Iniciar Sesión. Error 0001!</div>';
        }
    }
    
    static public function salir() {
        session_destroy();
        echo '<script>window.location = "index.php";</script>';
    }
    
    static public function valida_sesion() {
        //echo var_dump($_SESSION["usuario"]);
        if(isset($_SESSION["usuario"]) 
                and is_numeric($_SESSION["usuario"]["id"])
                and $_SESSION["usuario"]["logueado"]=="si" 
                and !empty($_SESSION["usuario"]["email"])){
            
        } else {
            Usuarios::salir();
        }
    }
    
    static public function listar($id) {
        if(isset($id) and $id!=null and is_numeric($id)){
            $respuesta = UsuariosModel::get_data("*", "id",$id);
            return $respuesta;
        } else {
            $respuesta = UsuariosModel::get_data("*", null,null);
            return $respuesta;
        }
        
    }
    
    static public function insert() {
        if($_POST["btn_agregar"]=="agregar"){
            $data["rut"]=strtolower(trim($_POST["rut"]));
            $data["email"]=strtolower(trim($_POST["email"]));
            $data["nombre"]=ucwords(strtolower(trim($_POST["nombre"])));
            $data["apellidop"]=ucwords(strtolower(trim($_POST["apellido"])));
            $data["clave"]=$_POST["clave"];
            $data["clave2"]=$_POST["clave2"];
            
            $validacion=new Validacion();
            if($validacion::validar_rut_chile($data["rut"])){
                $existerut= UsuariosModel::get_data("count(id) as total", "rut_usuario", $data["rut"]);
                if($existerut["total"]==0){
                    if($validacion::validar_email($data["email"])){
                        if($validacion::validar_nombre_usuario($data["nombre"])){
                            if($validacion::validar_apellido_usuario($data["apellidop"])){
                                if($validacion::validar_clave($data["clave"]) and $validacion::validar_clave($data["clave2"])){
                                    if($data["clave"]==$data["clave2"]){
                                        $datos=array("rut_usuario" => $data["rut"],
                                            "email_usuario" => $data["email"],
                                            "nombre_usuario" => $data["nombre"],
                                            "apellidop_usuario" => $data["apellidop"],
                                            "clave_usuario" => sha1($data["clave"]));
                                        $respuesta = UsuariosModel::insert($datos);
                                        if($respuesta["status"]=="ok"){
                                            //alerta indicando que el usuario fue agregado con exito
                                            echo '<script>Swal.fire({
                                                title: "Ok",
                                                text: "El Usuario fue agregado Exitosamente!",
                                                icon: "success",
                                                showCancelButton: false,
                                                confirmButtonColor: "#3085d6",
                                                cancelButtonColor: "#d33",
                                                confirmButtonText: "Aceptar!"
                                            }).then((result) => {
                                            if (result.value) {
                                                window.location.href="index.php?go=listar";
                                            }
                                            })</script>';
                                        } else {
                                            echo "<script>Swal.fire('Error', 'No ha sido posible agregar al Usuario. Inténtalo más tarde. Error 00548.', 'error')</script>";
                                        }
                                    } else {
                                        echo "<script>Swal.fire('Datos no coinciden', 'La Clave y la Repetición de Clave, no son iguales.', 'warning')</script>";
                                    }
                                } else {
                                    echo "<script>Swal.fire('Dato no válido', 'La Clave ingresada no es válida.', 'warning')</script>";
                                }
                            } else {
                                echo "<script>Swal.fire('Dato no válido', 'El Apellido ingresado no es válido.', 'warning')</script>";
                            }
                        } else {
                            echo "<script>Swal.fire('Dato no válido', 'El Nombre ingresado no es válido.', 'warning')</script>";
                        }
                    } else {
                        echo "<script>Swal.fire('Dato no válido', 'El Email ingresado no es válido.', 'warning')</script>";
                    }
                } else {
                    echo "<script>Swal.fire('Dato duplicado', 'El Rut ingresado ya se encuentra registrado. Intenta con otro Rut.', 'warning')</script>";
                }
            } else {
                echo "<script>Swal.fire('Dato no válido', 'El Rut ingresado no es válido.', 'warning')</script>";
            }
        }
    }
    
    static public function update($id) {
        if($_POST["btn_editar"]=="editar"){
            if(isset($id) and is_numeric($id) and $id>0){
                $data["id"]=trim($id);
                $data["email"]=strtolower(trim($_POST["email"]));
                $data["nombre"]=ucwords(strtolower(trim($_POST["nombre"])));
                $data["apellidop"]=ucwords(strtolower(trim($_POST["apellido"])));

                $validacion=new Validacion();
                $existeusuario= UsuariosModel::get_data("count(id) as total", "id", $data["id"]);
                if($existeusuario["total"]>0){
                    if($validacion::validar_email($data["email"])){
                        if($validacion::validar_nombre_usuario($data["nombre"])){
                            if($validacion::validar_apellido_usuario($data["apellidop"])){
                                $datos=array("id" => $data["id"],
                                    "email_usuario" => $data["email"],
                                    "nombre_usuario" => $data["nombre"],
                                    "apellidop_usuario" => $data["apellidop"]);
                                $respuesta = UsuariosModel::edit($datos);
                                if($respuesta["status"]=="ok"){
                                    //alerta indicando que el usuario fue agregado con exito
                                    echo '<script>Swal.fire({
                                        title: "Ok",
                                        text: "El Usuario fue editado Exitosamente!",
                                        icon: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#3085d6",
                                        cancelButtonColor: "#d33",
                                        confirmButtonText: "Aceptar!"
                                    }).then((result) => {
                                    if (result.value) {
                                        window.location.href="index.php?go=listar";
                                    }
                                    })</script>';
                                } else {
                                    echo "<script>Swal.fire('Error', 'No ha sido posible editar al Usuario. Inténtalo más tarde. Error 00549.', 'error')</script>";
                                }
                            } else {
                                echo "<script>Swal.fire('Dato no válido', 'El Apellido ingresado no es válido.', 'warning')</script>";
                            }
                        } else {
                            echo "<script>Swal.fire('Dato no válido', 'El Nombre ingresado no es válido.', 'warning')</script>";
                        }
                    } else {
                        echo "<script>Swal.fire('Dato no válido', 'El Email ingresado no es válido.', 'warning')</script>";
                    }
                } else {
                    echo "<script>Swal.fire('No Existe', 'No se puede encontrar al Usuario en el sistema. Intenta con otro Usuario', 'warning')</script>";
                }
            } else {
                echo "<script>Swal.fire('Dato no válido', 'No es posible Editar al Usuario, ya que el ID no es correcto', 'warning')</script>";
            }
        }
    }
    
    static public function updatePassword($id) {
        if($_POST["btn_cambiar"]=="cambiar"){
            
            if(isset($id) and is_numeric($id) and $id>0){
                
                $data["id"]=trim($id);
                $data["claveactual"]=$_POST["claveactual"];
                $data["clave"]=$_POST["clave"];
                $data["clave2"]=$_POST["clave2"];
                $validacion=new Validacion();
                $existeusuario= UsuariosModel::get_data("count(id) as total", "id", $data["id"]);
                if($existeusuario["total"]>0){
                    if($validacion::validar_clave($data["claveactual"])){
                        if($validacion::validar_clave($data["clave"])){
                            if($validacion::validar_clave($data["clave2"])){
                                if($data["clave"]==$data["clave2"]){
                                    if($data["claveactual"]!=$data["clave"]){
                                        $respuesta = UsuariosModel::get_data("clave_usuario", "id", $data["id"]);
                                        if($respuesta["clave_usuario"]==sha1($data["claveactual"])){
                                            $datos=array("id" => $data["id"],
                                                "clave_usuario" => sha1($data["clave"]));
                                            $respuesta = UsuariosModel::editPassword($datos);
                                            if($respuesta["status"]=="ok"){
                                                //alerta indicando que el usuario fue agregado con exito
                                                echo '<script>Swal.fire({
                                                    title: "Ok",
                                                    text: "La Clave fue cambiada exitosamente. Deberás iniciar sesión nuevamente!",
                                                    icon: "success",
                                                    showCancelButton: false,
                                                    confirmButtonColor: "#3085d6",
                                                    cancelButtonColor: "#d33",
                                                    confirmButtonText: "Aceptar!"
                                                }).then((result) => {
                                                if (result.value) {
                                                    window.location.href="index.php?go=salir";
                                                }
                                                })</script>';
                                            } else {
                                                echo "<script>Swal.fire('Error', 'No ha sido posible cambiar la clave. Inténtalo más tarde. Error 00589.', 'error')</script>";
                                            }
                                        } else {
                                            echo "<script>Swal.fire('Clave incorrecta', 'La clave actual no es correcta.', 'warning')</script>";
                                        }
                                    } else {
                                        echo "<script>Swal.fire('Dato no válido', 'La Nueva Clave no puede ser igual a la Clave Actual', 'warning')</script>";
                                    }
                                } else {
                                    echo "<script>Swal.fire('No coinciden', 'La Nueva Clave y la Repetición de la Nueva Clave no son iguales', 'warning')</script>";
                                }
                            } else {
                                echo "<script>Swal.fire('Dato no válido', 'La Repetición de la Nueva Clave, no es válida', 'warning')</script>";
                            }
                        } else {
                            echo "<script>Swal.fire('Dato no válido', 'La Nueva Clave no es válida', 'warning')</script>";
                        }
                    } else {
                        echo "<script>Swal.fire('Dato no válido', 'La Clave Actual no es válida', 'warning')</script>";
                    }
                } else {
                    echo "<script>Swal.fire('No Existe', 'No se puede encontrar al Usuario en el sistema. No se puede cambiar la clave', 'warning')</script>";
                }
            } else {
                echo "<script>Swal.fire('Dato no válido', 'No es posible Editar al Usuario, ya que el ID no es correcto', 'warning')</script>";
            }
        }
    }
    
    static public function delete($id) {
        if($_POST["btn_editar"]=="editar"){
            if(isset($id) and is_numeric($id) and $id>0){
                $data["id"]=trim($id);

                $validacion=new Validacion();
                $existeusuario= UsuariosModel::get_data("count(id) as total", "id", $data["id"]);
                if($existeusuario["total"]>0){
                    $datos=array("id" => $data["id"]);
                    $respuesta = UsuariosModel::delete($datos);
                    if($respuesta["status"]=="ok"){
                        //alerta indicando que el usuario fue agregado con exito
                        echo '<script>Swal.fire({
                            title: "Ok",
                            text: "El Usuario fue editado Exitosamente!",
                            icon: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Aceptar!"
                        }).then((result) => {
                        if (result.value) {
                            window.location.href="index.php?go=listar";
                        }
                        })</script>';
                    } else {
                        echo "<script>Swal.fire('Error', 'No ha sido posible editar al Usuario. Inténtalo más tarde. Error 00549.', 'error')</script>";
                    }
                } else {
                    echo "<script>Swal.fire('No Existe', 'No se puede encontrar al Usuario en el sistema. Intenta con otro Usuario', 'warning')</script>";
                }
            } else {
                echo "<script>Swal.fire('Dato no válido', 'No es posible Eliminar al Usuario, ya que el ID no es correcto', 'warning')</script>";
            }
        }
    }
    
    static public function delete_ajax($id) {
        if(isset($id) and is_numeric($id) and $id>0){
            $data["id"]=trim($id);

            $validacion=new Validacion();
            $existeusuario= UsuariosModel::get_data("count(id) as total", "id", $data["id"]);
            if($existeusuario["total"]>0){
                $datos=array("id" => $data["id"]);
                $respuesta = UsuariosModel::delete($datos);
                if($respuesta["status"]=="ok"){
                    header('Content-Type: application/json');
                    echo '{"status":"ok", '
                    . '"message":"Usuario eliminado con Exito"}';
                } else {
                    header('Content-Type: application/json');
                    echo '{"status":"error", '
                    . '"message":"'.$respuesta["message"].'"}';
                }
            } else {
                header('Content-Type: application/json');
                    echo '{"status":"error", '
                    . '"message":"El Usuario no existe"}';
            }
        } else {
            header('Content-Type: application/json');
                    echo '{"status":"error", '
                    . '"message":"El id del Usuario a eliminar no es válido"}';
        }
    }
}

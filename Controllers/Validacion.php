<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Validacion
 *
 * @author Jorge Loyola
 */
class Validacion {
    
    static public function validar_rut_chile($dato){
        $rut=$dato;
        $suma=0;
        if(strpos($rut,"-")==false){
            $RUT[0] = substr($rut, 0, -1);
            $RUT[1] = substr($rut, -1);
        }else{
            $RUT = explode("-", trim($rut));
        }
        $elRut = str_replace(".", "", trim($RUT[0]));
        $factor = 2;
        for($i = strlen($elRut)-1; $i >= 0; $i--){
            $factor = $factor > 7 ? 2 : $factor;
            $suma += $elRut{$i}*$factor++;
        }
        $resto = $suma % 11;
        $dv = 11 - $resto;
        if($dv == 11){
            $dv=0;
        }else if($dv == 10){
            $dv="k";
        }else{
            $dv=$dv;
        }
        
        
       if($dv == trim(strtolower($RUT[1]))){
           return true;
       }else{
           return false;
       }
    }
    
    static public function validar_email($dato) {
        if($dato<>""){
            if(filter_var($dato, FILTER_VALIDATE_EMAIL)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    static public function validar_alfanumericos($dato) {
        if($dato==""){
            return false;
        } else {
            if (ctype_alnum($dato)) {
                return true;
            } else {
                return false;
            }
        }
    }
    
    static public function validar_solo_texto($dato) {
        $texto = trim($dato);
        if($texto=="" && trim($texto)==""){
            return false;
        }else{
            $patron = '/^[a-zA-Z, ]*$/';
            if(preg_match($patron,$texto)){
                return true;   
            }else{
                return false;
            }
        }   
    }
    
    static public function validar_clave($dato) {
        if($dato<>""){
            if(strlen($dato)>=4 and strlen($dato)<31) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    static public function validar_nombre_usuario($dato) {
        if(!empty($dato)){
            if(strlen($dato)>=1 and strlen($dato)<=300){
                if(Validacion::validar_solo_texto($dato)){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    
    static public function validar_apellido_usuario($dato) {
        if(!empty($dato)){
            if(strlen($dato)>=1 and strlen($dato)<=300){
                if(Validacion::validar_solo_texto($dato)){
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

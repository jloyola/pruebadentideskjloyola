/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready( function () {
    $('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "No hay registros",
            "info": "Mostrando la página _PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros diposnibles",
            "infoFiltered": "(filtrado de _MAX_ registros totales)",
            "search": "Buscar"
        }
    });
} );


function eliminar_usuario(id, rut) {
    swal.fire({
        title: "¿Seguro?",
        text: "¿Eliminar al Usuario, Rut:" + rut + "?",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Si, Eliminar!",
        showDenyButton: true,
        denyButtonText: 'No Eliminar',
        cancelButtonText: "Cancelar!",
        closeOnConfirm: false
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            //Swal.fire('Eliminado!', '', 'success');
            
            var parametros = {
            "id" : id
            };
            $.ajax({
                // En data puedes utilizar un objeto JSON, un array o un query string
                data: parametros,
                //Cambiar a type: POST si necesario
                type: "GET",
                // Formato de datos que se espera en la respuesta
                dataType: "json",
                // URL a la que se enviará la solicitud Ajax
                url: "index.php?ajax=si&function=eliminar_usuario",
            })
             .done(function( response, textStatus, jqXHR ) {
//                 if ( console && console.log ) {
//                     console.log( "La solicitud se ha completado correctamente." );
//                 }
                //alert(response.status);
                if(response.status=="ok"){
                    Swal.fire({
                            title: "Ok",
                            text: "El Usuario fue eliminado Exitosamente!",
                            icon: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#3085d6",
                            cancelButtonColor: "#d33",
                            confirmButtonText: "Aceptar!"
                        }).then((result) => {
                        if (result.value) {
                            window.location.href="index.php?go=listar";
                        }
                        })
                }
                if(response.status=="error"){
                    swal.fire ( "Error" ,  "Ha ocurrido un error al Eliminar el Usuario. El mensaje obtenido es "+response.message ,  "error" );
                } 
             })
             .fail(function( jqXHR, textStatus, errorThrown ) {
                 if ( console && console.log ) {
                     console.log( "La solicitud a fallado: " +  textStatus);
                 }
                //swal.fire ( "Error" ,  "Ha ocurrido un error al Eliminar el Usuario. El mensaje obtenido es "+textStatus ,  "error" );
            });
            
            
            
        } else if (result.isDenied) {
          Swal.fire('El Usuario no fue eliminado', '', 'info');
        }
    });
}



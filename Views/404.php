<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="404 La página solicitada no existe">
    <meta name="author" content="Jorge Loyola Bravo">
    

    <link rel="canonical" href="https://getbootstrap.com/docs/4.6/examples/cover/">

    

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">



    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="assets/css/404.css" rel="stylesheet">
    
    <title>Prueba Dentidesk - Jorge Loyola Bravo</title>
  </head>
  <body class="text-center">
    
<div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

  <main role="main" class="inner cover">
    <h1 class="cover-heading">Error 404.</h1>
    <p class="lead">La página solicitada no existe.</p>
    <p class="lead">
        <a href="index.php?go=listar" class="btn btn-lg btn-secondary">Volver</a>
    </p>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner">
      <p>Prueba Técnica Dentidesk - Jorge Loyola Bravo</p>
    </div>
  </footer>
</div>


    
  </body>
</html>

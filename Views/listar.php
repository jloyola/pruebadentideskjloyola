<?php
Usuarios::valida_sesion();
$usuarios = Usuarios::listar(null);
?>
<?php include_once 'includes/header.php'; ?>

        <?php include_once 'includes/navbar.php'; ?>

        <div class="container-fluid">
          <div class="row">
            <?php include_once 'includes/sidebar.php'; ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <br>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">Bienvenido(a): <?php echo $_SESSION["usuario"]["nombre"] ?></li>
                      <li class="breadcrumb-item active" aria-current="page">Listar Usuarios</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">
                        Listar Usuarios
                    </div>
                    <div class="card-body">
                        <a href="index.php?go=agregar" class="btn btn-primary btn-sm">Nuevo Usuario</a><hr>
                        <?php 
                        if(count($usuarios)>0){
                        ?>
                        <div class="table-responsive">
                            <table class="table table-striped table-sm" id="table">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Rut</th>
                                    <th>Email</th>
                                    <th>Nombre Completo</th>
                                    <th>Opciones</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $cont=0;
                                    foreach ($usuarios as $key=>$value){
                                        $cont++;
                                    ?>
                                    <tr>
                                      <td><?php echo $cont ?></td>
                                      <td><?php echo $value["rut_usuario"] ?></td>
                                      <td><?php echo $value["email_usuario"] ?></td>
                                      <td><?php echo $value["nombre_usuario"]." ".$value["apellidop_usuario"] ?></td>
                                      <td>
                                          <a href="index.php?go=editar&user=<?php echo $value["id"] ?>" class="btn btn-warning btn-sm">Editar</a>
                                          <button onclick="eliminar_usuario('<?php echo $value["id"] ?>', '<?php echo $value["rut_usuario"] ?>')" type="button" class="btn btn-danger btn-sm">Eliminar</button>
                                      </td>
                                    </tr>
                                  <?php 
                                    }
                                  ?>

                                </tbody>
                            </table>
                        </div>
                        <?php 
                        } else {
                        ?>
                        <div class="alert alert-warning" role="alert">
                            <h4 class="alert-heading">Sin datos!</h4>
                            <p>No existen Usuarios Registrados, por favor agrega un Nuevo Usuario.</p>
                            <hr>
                            <a href="index.php?go=agregar" class="btn btn-primary">Agregar Usuario</a>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
              
              
              
            </main>
          </div>
        </div>
    
        
    <?php include_once 'includes/footer.php'; ?>
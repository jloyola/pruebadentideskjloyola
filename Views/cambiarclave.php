<?php 
Usuarios::valida_sesion();
$usuario=Usuarios::listar($_SESSION["usuario"]["id"]);
include_once 'includes/header.php'; 
?>

        <?php include_once 'includes/navbar.php'; ?>

        <div class="container-fluid">
          <div class="row">
            <?php include_once 'includes/sidebar.php'; ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <br>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">Bienvenido(a): <?php echo $_SESSION["usuario"]["nombre"] ?></li>
                      <li class="breadcrumb-item active" aria-current="page">Cambiar Clave</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">
                        Cambiar Clave Usuario
                    </div>
                    <div class="card-body">
                        <small class="obligatorio">(*) Campo Obligatorio</small>
                        <hr>
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php 
                                    Usuarios::updatePassword($_SESSION["usuario"]["id"]);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rut">Rut <span class="obligatorio">(*)</span></label>
                                        <input type="text" readonly="" class="form-control" id="rut" name="rut" aria-describedby="Rut" required="" value="<?php echo $usuario["rut_usuario"] ?>" disabled="">
                                        <small id="emailHelp" class="form-text text-muted">Sin puntos y con guión medio Ej:12345678-9</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Nombre <span class="obligatorio">(*)</span></label>
                                        <input type="email" readonly="" class="form-control" id="email" name="email" aria-describedby="Email" required="" value="<?php echo $usuario["email_usuario"] ?>">
                                        <small id="emailHelp" class="form-text text-muted">Ej: micorreo@micorreo.cl</small>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="claveactual">Clave Actual <span class="obligatorio">(*)</span></label>
                                        <input type="password" class="form-control" id="claveactual" name="claveactual" required="">
                                        <small id="emailHelp" class="form-text text-muted">Ingresa tu Clave Actual</small>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="clave">Clave <span class="obligatorio">(*)</span></label>
                                        <input type="password" class="form-control" id="clave" name="clave" required="">
                                        <small id="emailHelp" class="form-text text-muted">Se recomiendan Letras, Números y algún caracter especial</small>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="clave2">Repetir Clave <span class="obligatorio">(*)</span></label>
                                        <input type="password" class="form-control" id="clave2" name="clave2" required="">
                                        <small id="emailHelp" class="form-text text-muted">Repetir Clave</small>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-3 offset-3">
                                    <a class="btn btn-secondary btn-block" href="index.php?go=listar">Volver al Inicio</a>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary btn-block" id="btn_cambiar" name="btn_cambiar" value="cambiar">Cambiar Clave</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
          </div>
        </div>
    
        
    <?php include_once 'includes/footer.php'; ?>
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
              <div class="sidebar-sticky pt-3">
                <ul class="nav flex-column">
                  <li class="nav-item">
                    <a class="nav-link <?php if($_GET["go"]=="listar" or $_GET["go"]=="agregar" or $_GET["go"]=="editar"){ echo "active"; } ?>" href="index.php?go=listar">
                      <span data-feather="home"></span>
                      Listar <span class="sr-only">(current)</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link <?php if($_GET["go"]=="cambiarclave"){ echo "active"; } ?>" href="index.php?go=cambiarclave">
                      <span data-feather="home"></span>
                      Cambiar Mi Clave <span class="sr-only">(current)</span>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="index.php?go=salir">
                      <span data-feather="file"></span>
                      Salir
                    </a>
                  </li>
                  
                </ul>
              </div>
            </nav>
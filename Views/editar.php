<?php 
Usuarios::valida_sesion();
$usuario=Usuarios::listar($_GET["user"]);
include_once 'includes/header.php'; 
?>

        <?php include_once 'includes/navbar.php'; ?>

        <div class="container-fluid">
          <div class="row">
            <?php include_once 'includes/sidebar.php'; ?>
            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
                <br>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item">Bienvenido(a): <?php echo $_SESSION["usuario"]["nombre"] ?></li>
                      <li class="breadcrumb-item active" aria-current="page">Editar Usuario</li>
                    </ol>
                </nav>
                <div class="card">
                    <div class="card-header">
                        Editar Usuario
                    </div>
                    <div class="card-body">
                        <small class="obligatorio">(*) Campo Obligatorio</small>
                        <hr>
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12">
                                    <?php 
                                        Usuarios::update($_GET["user"]);
                                        
                                        //echo var_dump($usuario);
                                    ?>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rut">Rut <span class="obligatorio">(*)</span></label>
                                        <input type="text" class="form-control" id="rut" name="rut" aria-describedby="Rut" required="" value="<?php echo $usuario["rut_usuario"] ?>" disabled="">
                                        <small id="emailHelp" class="form-text text-muted">Sin puntos y con guión medio Ej:12345678-9</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email <span class="obligatorio">(*)</span></label>
                                        <input type="email" class="form-control" id="email" name="email" aria-describedby="Email" required="" value="<?php echo $usuario["email_usuario"] ?>">
                                        <small id="emailHelp" class="form-text text-muted">Ej: micorreo@micorreo.cl</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="nombre">Nombre <span class="obligatorio">(*)</span></label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="Nombre" required="" value="<?php echo $usuario["nombre_usuario"] ?>">
                                        <small id="emailHelp" class="form-text text-muted">Ej: Jorge</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="apellido">Apellido <span class="obligatorio">(*)</span></label>
                                        <input type="text" class="form-control" id="apellido" name="apellido" aria-describedby="Apellido" required="" value="<?php echo $usuario["apellidop_usuario"] ?>">
                                        <small id="emailHelp" class="form-text text-muted">Ej: Loyola</small>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <hr>
                                </div>
                                <div class="col-md-3 offset-3">
                                    <a class="btn btn-secondary btn-block" href="index.php?go=listar">Volver</a>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-primary btn-block" id="btn_editar" name="btn_editar" value="editar">Editar Usuario</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
          </div>
        </div>
    
        
    <?php include_once 'includes/footer.php'; ?>
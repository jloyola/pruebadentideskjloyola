<?php
require_once(__DIR__ . "/Conexion.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuarios
 *
 * @author Jorge Loyola
 */
class UsuariosModel {

    public static function get_data($function, $item, $valor) {
        if(isset($function) and !empty($function) and $function!=null){
            if($item == null and $valor == null){
                $stmt = Conexion::conectar()->prepare("SELECT $function FROM usuarios");
                $stmt->execute();
                return $stmt -> fetchAll();
            } else {
                $stmt = Conexion::conectar()->prepare("SELECT $function FROM usuarios WHERE $item = :$item");
                $stmt->bindParam(":".$item, $valor, PDO::PARAM_STR);
                $stmt->execute();
                return $stmt -> fetch();
            }

            $stmt->close();
            $stmt = null;
        } else {
            return false;
        }
    }
    
    static public function insert($datos) {
        $stmt = Conexion::conectar()->prepare("INSERT INTO usuarios(rut_usuario, email_usuario, nombre_usuario, apellidop_usuario, clave_usuario) VALUES (:rut, :email, :nombre, :apellido,:clave)");
        $stmt->bindParam(":rut", $datos["rut_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":email", $datos["email_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":nombre", $datos["nombre_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":apellido", $datos["apellidop_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":clave", $datos["clave_usuario"], PDO::PARAM_STR);
        if($stmt->execute()){
            $respuesta["status"]="ok";
            $respuesta["message"]="Datos insertados con Éxito";
            return $respuesta;
        }else{
            $respuesta["status"]="error";
            $respuesta["message"]=Conexion::conectar()->errorInfo();;
            return $respuesta;
        }
        $stmt->close();
        $stmt = null;	
    }
    
    static public function edit($datos){
        $stmt = Conexion::conectar()->prepare("UPDATE usuarios SET email_usuario=:email, nombre_usuario=:nombre, apellidop_usuario=:apellido WHERE id = :id");
        $stmt->bindParam(":email", $datos["email_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":nombre", $datos["nombre_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":apellido", $datos["apellidop_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

        if($stmt->execute()){
            $respuesta["status"]="ok";
            $respuesta["message"]="Datos editados con Éxito";
            return $respuesta;
        }else{
            $respuesta["status"]="error";
            $respuesta["message"]=Conexion::conectar()->errorInfo();;
            return $respuesta;
        }

        $stmt->close();
        $stmt = null;	
    }
    
    static public function editPassword($datos){
        $stmt = Conexion::conectar()->prepare("UPDATE usuarios SET clave_usuario=:clave WHERE id = :id");
        $stmt->bindParam(":clave", $datos["clave_usuario"], PDO::PARAM_STR);
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);

        if($stmt->execute()){
            $respuesta["status"]="ok";
            $respuesta["message"]="Clave Editada con Éxito";
            return $respuesta;
        }else{
            $respuesta["status"]="error";
            $respuesta["message"]=Conexion::conectar()->errorInfo();;
            return $respuesta;
        }

        $stmt->close();
        $stmt = null;	
    }
    
    static public function delete($datos) {
        $stmt = Conexion::conectar()->prepare("DELETE FROM usuarios WHERE id = :id");
        $stmt->bindParam(":id", $datos["id"], PDO::PARAM_INT);
        if($stmt->execute()){
            $respuesta["status"]="ok";
            $respuesta["message"]="Dato eliminado con Éxito";
            return $respuesta;
        }else{
            $respuesta["status"]="error";
            $respuesta["message"]=Conexion::conectar()->errorInfo();;
            return $respuesta;
        }

        $stmt->close();
        $stmt = null;	
    }
}

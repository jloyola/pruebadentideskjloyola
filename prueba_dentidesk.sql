-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-02-2021 a las 23:00:04
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba_dentidesk`
--
CREATE DATABASE IF NOT EXISTS `prueba_dentidesk` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `prueba_dentidesk`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `rut_usuario` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apellidop_usuario` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clave_usuario` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `rut_usuario`, `email_usuario`, `nombre_usuario`, `apellidop_usuario`, `clave_usuario`) VALUES
(2, '17320912-6', 'jorge.loyola.bravo@gmail.com', 'Jorge', 'Loyola', '8cb2237d0679ca88db6464eac60da96345513964'),
(6, '11111111-1', 'juanito@perez.cl', 'Juanito', 'Perez', '8cb2237d0679ca88db6464eac60da96345513964'),
(7, '9826077-3', 'sergio@perez.cl', 'Sergio', 'Perez', '8cb2237d0679ca88db6464eac60da96345513964'),
(8, '9890314-3', 'daniela@perez.cl', 'Daniela', 'Perez', '8cb2237d0679ca88db6464eac60da96345513964');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD UNIQUE KEY `usuario_id` (`id`),
  ADD KEY `rut_usuario` (`rut_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
